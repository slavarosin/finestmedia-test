$(function() {
	$(".datepicker").datepicker({
		dateFormat : 'dd.mm.yy'
	});

	var options = {
		dataType : 'json',
		success : function(response) {
			$("#result").html(response.result);
		},
		error : function(xhr, s, responseStatus, form) {
			console.log(xhr);
			console.log(form);
			var response = JSON.parse(xhr.responseText);
			for (i = 0; i < response.errors.length; i++) {
				var error = response.errors[i];

				// field error or main error
				if (error.fieldName != null) {
					console.log("!!!");
					var field = $("#" + error.fieldName);
					field.addClass("error-highlight");
					field.closest("div.field").append('<div class="error-message">' + error.message + '</div>');
				} else {
					console.log("asdasd");
					$("#error-block").html(error.message + "</br>");
				}
			}
		},
		beforeSubmit : function(arr, $form, options) {
			$("#result").empty();
			$("#error-block").empty();
			$(".error-message").remove();
			$(".error-highlight").removeClass("error-highlight");
		}
	};

	$("form").ajaxForm(options);

});